This is Spring Boot 2.3.9 (mysql) application, using jdk 8. 
Check in cmdline if jdk-8 is installed: java -version.
Create "air_companies" database
in mysql server with cmdline or MySQLWorkbench.
This database will be used to map jpa entities with
auto-generated tables.
To run:
Clone repo from bitbucket:
https://bitbucket.org/andriy1lan/air-company-management-system
I.In non-docker version -
then build with maven: mvn compile or mvn install 
and run spring boot on localhost:8080 with: mvn spring-boot:run
Or use Eclipse, Intellij IDEA or another IDE
to import, build and run of the project. 
After spring boot startup, tables will be
auto-generated, and populated initially from data.sql
(check if spring.datasource.initialization-mode=always is set in properties file).
The next time you run this spring boot app
put option spring.datasource.initialization-mode= from always to none.
You can check work of API endpoints
by importing Postman collections from project
root folder.
For the reasons of simplicity LocalDateTime java
class (default UTC time zone on 
server) is used for datetime type in db,
indeed it could be changed to OffsetDateTime,
that allow to save offset of time zone and then
to convert to usable ZonedLocalTime java type.
Due to technical restrictions jdk 8 is
used, indeed this project was compiled and
succesfully run using ZULU OpenJDK 11 (Java 11 version).
So just creating modules is needed for Java 11.

Provided endpoints:
1)AirCompany controller:
@GetMapping(value = "/companies")
@GetMapping(value = "/companies/flightsbystatus/{name}/{status}") -- by company name and status
@GetMapping(value = "/companies/{id}")
@PostMapping(value="/companies")
@PutMapping(value = "/companies/{id}")
@DeleteMapping("/companies/{id}")

2)AirPlane controller:
@GetMapping(value = "/planes")
@GetMapping(value = "/planes/{id}")
@PostMapping(value="/planes")
@PatchMapping(value="/planes/addtocompany/{id}/{company}") -- assign airplane
to company if comapny is not set intially
@PatchMapping(value="/planes/move/{id}/{company1}/{company2}") -- reassign 
airplane from one company to another company
@DeleteMapping("/planes/{id}")

3)Flight controller:
@GetMapping(value = "/flights")
@GetMapping(value = "/flights/{id}")
@GetMapping(value = "/flights/active24") -- get active flights that exceeds 24 hours
@GetMapping(value = "/flights/completedOvertime") -- get completed flight
whose estimated flight time is less with actual flight time.
@PostMapping(value="/flights") -- add flight with Pending status
@PatchMapping(value="/flights/status/{id}/{status}") -- change status (delayed,
active, completed) of flights with provided datetime
@DeleteMapping("/flights/{id}")

II.To use Docker for this project -
check if Docker is installed: docker --version
or install docker on your system.

1.Before building project and image change 
datasource property localhost in spring.datasource.url
to mysqlhost (in container):
spring.datasource.url=jdbc:mysql://mysqlhost:3306/air_companiesuseSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC
and add password for mysql db in container:
spring.datasource.password=pass

Create bridge-network for spring-boot
and mysql separate containers in cli:
docker network create bridge-network

2.Create mysql docker image-
Move to mysql dockerfile folder:
cd .\mysql-image\

And run docker command to create image from dockerfile:
docker image build mysqlimage .

Check if image created:
docker image ls

Now run the mysql image:
docker container run -d --net bridge-network -v mysql-storage:/var/lib/mysql --name mysqlhost mysqlimage

3.Move to the SpringBoot project
and create jar file:
mvn clean package

Then create springboot image there from dockerfile and jar:
docker image build -t aircompanyimage .

Now run spring boot container:
docker container run -p 8081:8081 --net bridge-network --name aircompanyapi aircompanyimage

4.Open web browser and access this aircomopany api
- If spring boot does not starts correctly
try to add this option Dspring.profiles.active=
e.g.
ENTRYPOINT ["java","-Dspring.profiles.active=dev","-jar","/app.jar"]
This Docker configuration needed to test and run 
on 64-bit OS with Docker installed as it is unable
to run on 32-bit.
