package com.koblan.aircompaniesapi.exceptions;

public class NoSuchAirPlaneInCompanyException extends Exception {
	private static final long serialVersionUID = 1L;
	public NoSuchAirPlaneInCompanyException() {}
	public NoSuchAirPlaneInCompanyException(String message) {
		super("There is no such airplane in "+message);
	}
}
