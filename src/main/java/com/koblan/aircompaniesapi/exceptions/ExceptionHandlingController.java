package com.koblan.aircompaniesapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.koblan.aircompaniesapi.models.Message;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneInCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchFlightException;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(NoSuchAirCompanyException.class)
    ResponseEntity<Message> showNoSuchAirCompanyException(){
        return new ResponseEntity<Message>(new Message("Such air-company not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(NoSuchAirPlaneException.class)
    ResponseEntity<Message> showNoSuchAirPlaneException(){
        return new ResponseEntity<Message>(new Message("Such air-plane not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(NoSuchAirPlaneInCompanyException.class)
    ResponseEntity<Message> showNoAirPlaneInCompanyException(NoSuchAirPlaneInCompanyException ex){
        return new ResponseEntity<Message>(new Message("Such air-plane in company not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(NoSuchFlightException.class)
    ResponseEntity<Message> showNoSuchFlightException(){
        return new ResponseEntity<Message>(new Message("Such flight not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(Exception.class)
    ResponseEntity<Message> showException(Exception ex){
        return new ResponseEntity<Message>(new Message(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

	
	
}
