package com.koblan.aircompaniesapi.exceptions;

public class NoSuchAirCompanyException extends Exception {
	private static final long serialVersionUID = 1L;
	public NoSuchAirCompanyException() {}
	public NoSuchAirCompanyException(String message) {
		super(message);
	}
}