package com.koblan.aircompaniesapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.koblan.aircompaniesapi.exceptions.NoSuchAirCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneInCompanyException;
import com.koblan.aircompaniesapi.models.AirCompany;
import com.koblan.aircompaniesapi.models.AirPlane;
import com.koblan.aircompaniesapi.models.Flight;
import com.koblan.aircompaniesapi.services.AirCompanyService;
import com.koblan.aircompaniesapi.services.AirPlaneService;

@RestController
public class AirPlaneController {
	 
	 @Autowired
	 AirPlaneService airPlaneService;
	 
	 @GetMapping(value = "/planes")
	 public ResponseEntity<List<AirPlane>> getAllAirPlanes() throws NoSuchAirPlaneException {
	        List<AirPlane> airPlanesList = airPlaneService.getAllPlanes();
	        return new ResponseEntity<>(airPlanesList, HttpStatus.OK);
	 }
	 
	 @GetMapping(value = "/planes/{id}")
	 public ResponseEntity<AirPlane> getAirPlane(@PathVariable Long id) throws NoSuchAirPlaneException {
		    AirPlane airPlane=airPlaneService.getAirPlane(id);
		    if (airPlane==null) throw new NoSuchAirPlaneException();
	        return new ResponseEntity<>(airPlane, HttpStatus.OK);
	 }
	 
	 @PostMapping(value="/planes")
     public ResponseEntity<AirPlane> addAirPlane(@RequestBody AirPlane airPlane) {
		     AirPlane newPlane=airPlaneService.createAirPlane(airPlane);
			 return new ResponseEntity<>(newPlane, HttpStatus.CREATED);
     }
	 
	 @PatchMapping(value="/planes/addtocompany/{id}/{company}")
     public ResponseEntity<AirPlane> addAirCompanyToPlane(@PathVariable Long id, @PathVariable String company) 
    		 throws NoSuchAirPlaneException, NoSuchAirCompanyException {
		     airPlaneService.addToAirCompany(company, id);
			 return new ResponseEntity<>(HttpStatus.OK);
     }
	 
	 @PatchMapping(value="/planes/move/{id}/{company1}/{company2}")
     public ResponseEntity<String> addAirCompanyToPlane(@PathVariable Long id, @PathVariable String company1, @PathVariable String company2) 
    		 throws NoSuchAirPlaneException, NoSuchAirCompanyException,NoSuchAirPlaneInCompanyException {
		     airPlaneService.moveAirPlanesToAnotherAirCompany(id, company1, company2);
		     return new ResponseEntity<>("Airplane moved from "+company1+" to "+company2+"",HttpStatus.OK);
     }
	 
	 @DeleteMapping("/planes/{id}")
	 public ResponseEntity<?> deletePlane(@PathVariable Long id) throws NoSuchAirPlaneException {
		    airPlaneService.deletePlane(id);
			return new ResponseEntity<>(HttpStatus.OK);
     }

}
