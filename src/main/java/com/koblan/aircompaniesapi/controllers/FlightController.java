package com.koblan.aircompaniesapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.koblan.aircompaniesapi.exceptions.NoSuchAirCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneInCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchFlightException;
import com.koblan.aircompaniesapi.models.AirCompany;
import com.koblan.aircompaniesapi.models.AirPlane;
import com.koblan.aircompaniesapi.models.Flight;
import com.koblan.aircompaniesapi.models.StatusDateTime;
import com.koblan.aircompaniesapi.services.AirCompanyService;
import com.koblan.aircompaniesapi.services.FlightService;

@RestController
public class FlightController {

	 @Autowired
	 FlightService flightService;
	 
	 @GetMapping(value = "/flights")
	 public ResponseEntity<List<Flight>> getAllFlights() {
	        List<Flight> flightsList = flightService.getAllFlights();
	        return new ResponseEntity<>(flightsList, HttpStatus.OK);
	 }
	 
	 @GetMapping(value = "/flights/{id}")
	 public ResponseEntity<Flight> getFlight(@PathVariable Long id) throws NoSuchFlightException {
		    Flight flight=flightService.getFlight(id);
		    if (flight==null) throw new NoSuchFlightException();
	        return new ResponseEntity<>(flight, HttpStatus.OK);
	 }
	 
	 @GetMapping(value = "/flights/active24")
	 public ResponseEntity<List<Flight>> getFlightsWithActiveStatus24() {
	        List<Flight> flightsList = flightService.getFlightsWithActiveStatus();
	        return new ResponseEntity<>(flightsList, HttpStatus.OK);
	 }
	 
	 @GetMapping(value = "/flights/completedOvertime")
	 public ResponseEntity<List<Flight>> getCompletedFlightsWithExcededFlightTime() {
	        List<Flight> flightsList = flightService.getCompletedFlightsWithOverTime();
	        return new ResponseEntity<>(flightsList, HttpStatus.OK);
	 }
	 
	 @PostMapping(value="/flights")
     public ResponseEntity<Flight> addAirFlight(@RequestBody Flight flight) throws NoSuchAirPlaneInCompanyException {
		     Flight newFlight=flightService.createFlight(flight);
			 return new ResponseEntity<>(newFlight, HttpStatus.CREATED);
     }
	 
	 @PatchMapping(value="/flights/status/{id}/{status}")
     public ResponseEntity<String> changeStatus(@PathVariable Long id, @PathVariable String status, @RequestBody StatusDateTime dateTime) throws NoSuchFlightException {
		     flightService.changeStatus(status, dateTime.getDate(), id);
			 return new ResponseEntity<>("Flight set status to "+status+"", HttpStatus.OK);
     }
	 
	 @DeleteMapping("/flights/{id}")
	 public ResponseEntity<?> deleteFlight(@PathVariable Long id) throws NoSuchFlightException {
		    flightService.deleteFlight(id);
			return new ResponseEntity<>(HttpStatus.OK);
     }
}