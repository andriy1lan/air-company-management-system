package com.koblan.aircompaniesapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.koblan.aircompaniesapi.exceptions.NoSuchAirCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneInCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchFlightException;
import com.koblan.aircompaniesapi.models.AirCompany;
import com.koblan.aircompaniesapi.models.AirPlane;
import com.koblan.aircompaniesapi.models.Flight;
import com.koblan.aircompaniesapi.repositories.AirCompanyRepository;
import com.koblan.aircompaniesapi.repositories.AirPlaneRepository;
import com.koblan.aircompaniesapi.services.AirCompanyService;
import com.koblan.aircompaniesapi.services.AirPlaneService;

@RestController
public class AirCompanyController {

	 @Autowired
	 AirCompanyService airCompaniesService;
	 
	 @Autowired
	 AirPlaneService airPlaneService;
	 
	 @GetMapping(value = "/companies")
	 public ResponseEntity<List<AirCompany>> getAllAirCompanies() throws NoSuchAirCompanyException {
	        List<AirCompany> airCompaniesList = airCompaniesService.getAllAirCompanies();
	        return new ResponseEntity<>(airCompaniesList, HttpStatus.OK);
	 }
	 
	 @GetMapping(value = "/companies/flightsbystatus/{name}/{status}")
	 public ResponseEntity<List<Flight>> getFlightsByStatus(@PathVariable String name, @PathVariable String status) 
			throws NoSuchAirCompanyException,NoSuchFlightException {
	        List<Flight> flights = airCompaniesService.getFlightsByAirCompanyNameAndStatus(name, status);
	        if (flights==null) throw new NoSuchFlightException();
	        return new ResponseEntity<>(flights, HttpStatus.OK);
	 }
	 
	 @GetMapping(value = "/companies/{id}")
	 public ResponseEntity<AirCompany> getAirCompany(@PathVariable Long id) throws NoSuchAirCompanyException {
		    AirCompany airCompany=airCompaniesService.getAirCompany(id);
	        return new ResponseEntity<>(airCompany, HttpStatus.OK);
	 }
	 
	 @PostMapping(value="/companies")
     public ResponseEntity<AirCompany> addAirCompany(@RequestBody AirCompany airCompany) {
		     AirCompany newCompany=airCompaniesService.createAirCompany(airCompany);
			 return new ResponseEntity<>(newCompany, HttpStatus.CREATED);
     }
	 
	 @PutMapping(value = "/companies/{id}")
	 public  ResponseEntity<AirCompany> updateAirCompany(@RequestBody AirCompany airCompany, @PathVariable Long id) throws NoSuchAirCompanyException,Exception {
		    airCompaniesService.updateAirCompany(airCompany, id);
	        return new ResponseEntity<>(HttpStatus.OK);
	 }
	 
	 @DeleteMapping("/companies/{id}")
	 public ResponseEntity<?> deleteAirCompany(@PathVariable Long id) throws NoSuchAirCompanyException {
		    airCompaniesService.deleteAirCompany(id);
			return new ResponseEntity<>(HttpStatus.OK);
     }
	 
	 
}
