package com.koblan.aircompaniesapi.services;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneException;
import com.koblan.aircompaniesapi.models.AirCompany;
import com.koblan.aircompaniesapi.models.AirPlane;
import com.koblan.aircompaniesapi.models.Flight;
import com.koblan.aircompaniesapi.models.FlightStatus;
import com.koblan.aircompaniesapi.repositories.AirCompanyRepository;

@Service
public class AirCompanyService {
	
	@Autowired
	AirCompanyRepository airCompanyRepo;
	
	public List<AirCompany> getAllAirCompanies() {
        return airCompanyRepo.findAll();
    }
	
	public AirCompany getAirCompany(Long id) throws NoSuchAirCompanyException {
		Optional<AirCompany> company = airCompanyRepo.findById(id);
        if (!company.isPresent()) throw new NoSuchAirCompanyException();
        return company.get();
    }
	
	public AirCompany getAirCompanyByName(String name) {
		Optional<AirCompany> company = airCompanyRepo.findByName(name);
        if (!company.isPresent()) return null;
        return company.get();
    }
	
	public List<Flight> getFlightsByAirCompanyNameAndStatus(String name, String status)
			throws NoSuchAirCompanyException {
		Optional<AirCompany> company = airCompanyRepo.findByName(name);
        if (!company.isPresent()) throw new NoSuchAirCompanyException();
        AirCompany airCompany=company.get();
        List<Flight> flights=airCompany.getFlights().stream().
        		filter(f->f.getFlightStatus().toString().equalsIgnoreCase(status)).collect(Collectors.toList());
        return flights;
    }
	
	@Transactional
    public AirCompany createAirCompany(AirCompany airCompany) {
		AirCompany company=null;
		try{
		company=airCompanyRepo.save(airCompany);
		}
		catch (Exception ex) {
		}
		return company;
    }
	
	@Transactional
    public void updateAirCompany(AirCompany airCompany, Long id) throws NoSuchAirCompanyException,Exception {
		Optional<AirCompany> company = airCompanyRepo.findById(id);
        if (!company.isPresent()) throw new NoSuchAirCompanyException();
        AirCompany airCompany0=company.get();
        airCompany0.setName(airCompany.getName());
        airCompany0.setCompanyType(airCompany.getCompanyType());
        airCompany0.setFoundedAt(airCompany.getFoundedAt());
        if (airCompany.getAirPlanes()!=null) airCompany.getAirPlanes().forEach(ap->
        {if (!airCompany0.getAirPlanes().contains(ap)) ap.setAirCompany(airCompany0);});
        if (airCompany0.getAirPlanes()!=null) airCompany0.getAirPlanes().forEach(ap->
        {if (!airCompany.getAirPlanes().contains(ap)) airCompany0.getAirPlanes().remove(ap);});
        
        if (airCompany.getFlights()!=null) airCompany.getFlights().forEach(f->
        {if (!airCompany0.getFlights().contains(f)) f.setAirCompany(airCompany0);});
        if (airCompany0.getFlights()!=null) airCompany0.getFlights().forEach(f->
        {if (!airCompany.getFlights().contains(f)) airCompany0.getFlights().remove(f);});
        try{
        airCompanyRepo.save(airCompany0);
        }
        catch (Exception ex) {
        	//Exception exx=(Exception)ex.getCause();
        	throw (Exception)ex.getCause();
        }
	}
	
	@Transactional
    public void deleteAirCompany(Long id) throws NoSuchAirCompanyException {
		Optional<AirCompany> company = airCompanyRepo.findById(id);
        if (!company.isPresent()) throw new NoSuchAirCompanyException();
        airCompanyRepo.delete(company.get());
    }


}
