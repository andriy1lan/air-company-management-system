package com.koblan.aircompaniesapi.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.koblan.aircompaniesapi.exceptions.NoSuchAirCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneInCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchFlightException;
import com.koblan.aircompaniesapi.models.AirCompany;
import com.koblan.aircompaniesapi.models.AirPlane;
import com.koblan.aircompaniesapi.models.Flight;
import com.koblan.aircompaniesapi.models.FlightStatus;
import com.koblan.aircompaniesapi.repositories.AirPlaneRepository;
import com.koblan.aircompaniesapi.repositories.FlightRepository;

@Service
public class FlightService {
	
	@Autowired
	FlightRepository flightRepo;
	
	@Autowired
	AirPlaneRepository airPlaneRepo;
	
	public List<Flight> getAllFlights() {
        return flightRepo.findAll();
    }
	
	public Flight getFlight(Long id) {
		Optional<Flight> flight = flightRepo.findById(id);
        if (!flight.isPresent()) return null;
        return flight.get();
    }
	
	public List<Flight> getFlightsWithActiveStatus() {
        return flightRepo.findAll().stream().filter(f->f.getFlightStatus().equals(FlightStatus.ACTIVE))
        		.filter(f->f.getStartedAt().plusHours(24).isBefore(LocalDateTime.now())).collect(Collectors.toList());
    }

	public List<Flight> getCompletedFlightsWithOverTime() {
        return flightRepo.findAll().stream().filter(f->f.getFlightStatus().equals(FlightStatus.COMPLETED))
        		.filter(f->f.getStartedAt().plusMinutes((long)f.getFlightTime()).isBefore(f.getStartedAt())).collect(Collectors.toList());
    }
	
	
	
	@Transactional
    public Flight createFlight(Flight flight) throws NoSuchAirPlaneInCompanyException {
		Flight newFlight=null;
		flight.setFlightStatus(FlightStatus.PENDING);
		AirPlane plane=airPlaneRepo.findById(flight.getAirPlane().getId());
		if (plane.getAirCompany().getId()!=flight.getAirCompany().getId()) throw
		new NoSuchAirPlaneInCompanyException();
		newFlight=flightRepo.save(flight);
		return newFlight;
    }
	
	@Transactional 
    public void changeStatus(String status, LocalDateTime dateTime, Long id) throws NoSuchFlightException {
		Optional<Flight> optionalFlight = flightRepo.findById(id);
        if (!optionalFlight.isPresent()) throw new NoSuchFlightException();
        Flight flight=optionalFlight.get();
        if (FlightStatus.ACTIVE.toString().equalsIgnoreCase(status))
        {
        	flight.setFlightStatus(FlightStatus.ACTIVE);
        	flight.setStartedAt(dateTime);
        }
        else if (FlightStatus.COMPLETED.toString().equalsIgnoreCase(status))
        {
        	flight.setFlightStatus(FlightStatus.COMPLETED);
        	flight.setEndedAt(dateTime);
        }
        else if (FlightStatus.DELAYED.toString().equalsIgnoreCase(status)) {
        	flight.setFlightStatus(FlightStatus.DELAYED);
        	flight.setDelayStartedAt(dateTime);
        }
        flightRepo.save(flight);
	}
	
	@Transactional
    public void deleteFlight(Long id) throws NoSuchFlightException {
		Optional<Flight> flight = flightRepo.findById(id);
        if (!flight.isPresent()) throw new NoSuchFlightException();
        flightRepo.delete(flight.get());
    }

}
