package com.koblan.aircompaniesapi.services;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.koblan.aircompaniesapi.exceptions.NoSuchAirCompanyException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneException;
import com.koblan.aircompaniesapi.exceptions.NoSuchAirPlaneInCompanyException;
import com.koblan.aircompaniesapi.models.AirCompany;
import com.koblan.aircompaniesapi.models.AirPlane;
import com.koblan.aircompaniesapi.repositories.AirCompanyRepository;
import com.koblan.aircompaniesapi.repositories.AirPlaneRepository;

@Service
public class AirPlaneService {

	@Autowired
	AirPlaneRepository airPlaneRepo;
	
	@Autowired
	AirCompanyRepository airCompanyRepo;
	
	public List<AirPlane> getAllPlanes() {
        return airPlaneRepo.findAll();
    }
	
	public AirPlane getAirPlane(Long id) {
		Optional<AirPlane> plane = airPlaneRepo.findById(id);
        if (!plane.isPresent()) return null;
        return plane.get();
    }
	
	@Transactional
    public AirPlane createAirPlane(AirPlane airPlane) {
		AirPlane plane=null;
		try{
		plane=airPlaneRepo.save(airPlane);
		}
		catch (Exception ex) {
		}
		return plane;
    }
	
	@Transactional
    public void addToAirCompany(String airCompanyName, Long id)
    		throws NoSuchAirPlaneException,NoSuchAirCompanyException {
		Optional<AirPlane> plane = airPlaneRepo.findById(id);
        if (!plane.isPresent()) throw new NoSuchAirPlaneException();
        AirPlane airPlane=plane.get();
        Optional<AirCompany> company=airCompanyRepo.findByName(airCompanyName);
        if (!company.isPresent()) throw new NoSuchAirCompanyException();
        AirCompany airCompany=company.get();
        airPlane.setAirCompany(airCompany);
        airCompany.getAirPlanes().add(airPlane);
        airPlaneRepo.save(airPlane);
	}
	
	@Transactional
	public void moveAirPlanesToAnotherAirCompany(Long id, String company1, @PathVariable String company2) 
			 throws NoSuchAirCompanyException,NoSuchAirPlaneException, NoSuchAirPlaneInCompanyException {
		 if (getAirPlane(id)==null) throw new NoSuchAirPlaneException();
		 AirPlane airPlane=getAirPlane(id);
		 AirCompany airCompany1=airCompanyRepo.findByName(company1).get();
		 if (airCompany1==null) throw new NoSuchAirCompanyException();
		 AirCompany airCompany2=airCompanyRepo.findByName(company2).get();
		 if (airCompany2==null) throw new NoSuchAirCompanyException(); 
		 if (!airCompany1.getAirPlanes().contains(airPlane)) throw new NoSuchAirPlaneInCompanyException(company1);
		 airCompany1.getAirPlanes().remove(airPlane);
		 airCompany2.getAirPlanes().add(airPlane);
		 airCompanyRepo.save(airCompany1);
		 airCompanyRepo.save(airCompany2);
		 airPlane.setAirCompany(airCompany2);
	 }
	
	@Transactional
    public void deletePlane(Long id) throws NoSuchAirPlaneException {
		Optional<AirPlane> plane = airPlaneRepo.findById(id);
        if (!plane.isPresent()) throw new NoSuchAirPlaneException();
        airPlaneRepo.delete(plane.get());
    }
	
}
