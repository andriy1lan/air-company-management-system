package com.koblan.aircompaniesapi.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.koblan.aircompaniesapi.models.Flight;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {
	   Flight findById(long id);
	   List<Flight> findAll();
}
