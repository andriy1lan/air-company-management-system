package com.koblan.aircompaniesapi.repositories;
import com.koblan.aircompaniesapi.models.AirCompany;
import com.koblan.aircompaniesapi.models.AirPlane;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirCompanyRepository extends JpaRepository<AirCompany, Long> {
	AirCompany findById(long id);
	Optional<AirCompany> findByName(String name);
}
