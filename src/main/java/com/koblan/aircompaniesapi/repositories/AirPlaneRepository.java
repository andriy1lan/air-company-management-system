package com.koblan.aircompaniesapi.repositories;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.koblan.aircompaniesapi.models.AirPlane;

@Repository
public interface AirPlaneRepository extends JpaRepository<AirPlane, Long>{
	AirPlane findById(long id);
}
