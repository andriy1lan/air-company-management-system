package com.koblan.aircompaniesapi.models;

public enum FlightStatus {
	ACTIVE, COMPLETED, DELAYED, PENDING
}
