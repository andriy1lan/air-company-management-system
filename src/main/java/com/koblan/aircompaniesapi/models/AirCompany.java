package com.koblan.aircompaniesapi.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class AirCompany {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(unique = true)
	private String name;
	private String companyType; //Regular/Standart, Budget/Lowcost
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate foundedAt;
	//@JsonManagedReference
	@JsonIgnore
	@OneToMany(mappedBy = "airCompany", fetch = FetchType.LAZY)
	private Set<AirPlane> airPlanes;
	@JsonIgnore
	@OneToMany(mappedBy = "airCompany", fetch = FetchType.LAZY)
	private Set<Flight> flights;
	
	public long getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompanyType() {
		return companyType;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	public LocalDate getFoundedAt() {
		return foundedAt;
	}
	public void setFoundedAt(LocalDate foundedAt) {
		this.foundedAt = foundedAt;
	}
	public Set<AirPlane> getAirPlanes() {
		return airPlanes;
	}
	public void setAirPlanes(Set<AirPlane> airPlanes) {
		this.airPlanes = airPlanes;
	}
	public Set<Flight> getFlights() {
		return flights;
	}
	public void setFlights(Set<Flight> flights) {
		this.flights = flights;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AirCompany other = (AirCompany) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
