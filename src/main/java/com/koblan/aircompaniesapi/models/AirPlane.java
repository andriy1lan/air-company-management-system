package com.koblan.aircompaniesapi.models;
import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class AirPlane {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String serialNumber;
    //@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "airCompany_id", nullable = true) //allow to add company later
	private AirCompany airCompany;
	private int numberOfFlights;
	private int flightDistance;
	private int fuelCapacity;
	private String type; //Light Jet, Mid-Size Jet, Jumbo-Jet
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate createdAt;
	@JsonIgnore
	@OneToMany(mappedBy = "airPlane", fetch = FetchType.LAZY)
	private Set<Flight> flights;
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public AirCompany getAirCompany() {
		return airCompany;
	}
	public void setAirCompany(AirCompany airCompany) {
		this.airCompany = airCompany;
	}
	public int getNumberOfFlights() {
		return numberOfFlights;
	}
	public void setNumberOfFlights(int numberOfFlights) {
		this.numberOfFlights = numberOfFlights;
	}
	public int getFlightDistance() {
		return flightDistance;
	}
	public void setFlightDistance(int flightDistance) {
		this.flightDistance = flightDistance;
	}
	public int getFuelCapacity() {
		return fuelCapacity;
	}
	public void setFuelCapacity(int fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public LocalDate getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(LocalDate createdAt) {
		this.createdAt = createdAt;
	}
	public Set<Flight> getFlights() {
		return flights;
	}
	public void setFlights(Set<Flight> flights) {
		this.flights = flights;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AirPlane other = (AirPlane) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
