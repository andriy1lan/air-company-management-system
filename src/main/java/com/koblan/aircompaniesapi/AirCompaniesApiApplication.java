package com.koblan.aircompaniesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirCompaniesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirCompaniesApiApplication.class, args);
	}

}
